import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Dtuser } from '../models/dtuser.interface';

//selector - app-user-form - elemento para reutilizar el componente en otros lugares
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
// Trabajando con formularios reactivos
  userForm: FormGroup;
// Agregando interfaz
// dtuser: es la propiedad donde vienen todos los datos
  dtuser: Dtuser;

  private isEmail = /\S+@\S+\.\S+/;

  // private fb: FormBuilder para trabajar con la funcion private initForm()
  constructor(private router: Router, private fb: FormBuilder) {
    const navigation = this.router.getCurrentNavigation();

    // propiedades opcionales
    this.dtuser = navigation?.extras?.state?.value;

    // llamar definicion del formulario - instancia del formulario - para asociar al html
    this.initForm();
  }

  // ---------------------------------------------------
  // Si se recarga la pagina nuevamente, dirigira a newu
  ngOnInit(): void {

    // confirmar si hay un usuario - 1:03:00
    if(typeof this.dtuser === 'undefined'){
      // Redirigir a la ruta - crear nuevo usuario
      // this.router.navigate(['edit']);
      this.router.navigate(['newu']);
    }else {
      this.userForm.patchValue(this.dtuser);
    }
  }
  // ---------------------------------------------------

  // this.userForm.value  - imprimir los valores (datos)d del formulario en el console browser
  onSave(): void {
    console.log('saved', this.userForm.value);
  }

  // metodo inicializar formulario - para asociar los datos con los campos en .html - formControlName (directivas)
  private initForm(): void {
    // form definition
    this.userForm = this.fb.group({
      nombre: ['', [Validators.required]],
      apellido:['', [Validators.required]],
      email:['', [Validators.required, Validators.pattern(this.isEmail)]],
      fechaIngreso: ['', [Validators.required]],
    });
  }

  onGoBackToList(): void {
    this.router.navigate(['List']);
  }
}
