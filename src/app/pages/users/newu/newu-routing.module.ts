import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewuComponent } from './newu.component';

const routes: Routes = [{ path: '', component: NewuComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewuRoutingModule { }
