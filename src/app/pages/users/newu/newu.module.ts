import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewuRoutingModule } from './newu-routing.module';
import { NewuComponent } from './newu.component';

// para trabajar con el user-form.components
import { UserFormModule } from 'src/app/shared/components/user-form/user-form.module';


@NgModule({
  declarations: [
    NewuComponent
  ],
  imports: [
    CommonModule,
    NewuRoutingModule,
    UserFormModule
  ]
})
export class NewuModule { }
