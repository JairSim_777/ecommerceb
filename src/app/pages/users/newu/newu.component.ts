import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-newu',
  template: `<app-user-form></app-user-form>`,
  styleUrls: ['./newu.component.scss']
})
export class NewuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
