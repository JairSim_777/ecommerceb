import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewuComponent } from './newu.component';

describe('NewuComponent', () => {
  let component: NewuComponent;
  let fixture: ComponentFixture<NewuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
