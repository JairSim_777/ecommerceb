import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisteruRoutingModule } from './registeru-routing.module';
import { RegisteruComponent } from './registeru.component';


@NgModule({
  declarations: [
    RegisteruComponent
  ],
  imports: [
    CommonModule,
    RegisteruRoutingModule
  ]
})
export class RegisteruModule { }
