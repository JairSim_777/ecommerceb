import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisteruComponent } from './registeru.component';

const routes: Routes = [{ path: '', component: RegisteruComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisteruRoutingModule { }
