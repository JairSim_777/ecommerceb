import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteruComponent } from './registeru.component';

describe('RegisteruComponent', () => {
  let component: RegisteruComponent;
  let fixture: ComponentFixture<RegisteruComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisteruComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
