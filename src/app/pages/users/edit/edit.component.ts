import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { Dtuser } from 'src/app/shared/components/models/dtuser.interface';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

// componente destino
export class EditComponent implements OnInit {

// Trabajar con formularios reactivos
userForm: FormGroup;

// employee - dtuser
// dtuser: any = null;

// Agregando interfaz
  dtuser: Dtuser;

  // propiedad para validadores email - expresion regular /\S+@\S+\.\S+/;
  private isEmail = /\S+@\S+\.\S+/;

  // inyeccion de dependleoPerez@gmail.comencias
  // private fb: FormBuilder para trabajar con la funcion private initForm()
  constructor(private router: Router, private fb: FormBuilder) {
    const navigation = this.router.getCurrentNavigation();

    // propiedades opcionales
    // employee - dtuser - value: es la propiedad donde vienen todos los datos
    this.dtuser = navigation?.extras?.state?.value;
    // this.dtuser = navigation?.extras?.state?.dtuser;

      // llamar definicion del formulario - instancia del formulario - para asociar al html
      this.initForm();
  }

  // ---------------------------------------------------
  // Si se recarga la pagina nuevamente
  ngOnInit(): void {

    // confirmar si hay un usuario - 1:03:00
    if(typeof this.dtuser === 'undefined'){
      // Redirigir a la ruta - crear nuevo usuario
      // this.router.navigate(['edit']);
      this.router.navigate(['newu']);
    }else {
      this.userForm.patchValue(this.dtuser);
    }
  }
  // ---------------------------------------------------

  // this.userForm.value  - imprimir los valores (datos) del formulario en el console browser
  onSave(): void {
    console.log('saved', this.userForm.value);
  }

  // metodo inicializar formulario - para asociar los datos con los campos en .html - formControlName (directivas)
  private initForm(): void {
    // form definition
    this.userForm = this.fb.group({
      nombre: ['', [Validators.required]],
      apellido:['', [Validators.required]],
      email:['', [Validators.required, Validators.pattern(this.isEmail)]],
      fechaIngreso: ['', [Validators.required]],
    });
  }

  onGoBackToList(): void {
    this.router.navigate(['List']);
  }

}
