import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  fakeData = [
    {
      nombre: 'Jair ',
      apellido: 'Simba',
      email: 'jsvingenielectro@gmail.com',
      fechaIngreso: '28/03/2020'
    },
    {
      nombre: 'josept ',
      apellido: 'Perez',
      email: 'javp@gmail.com',
      fechaIngreso: '12/02/2019'
    },
    {
      nombre: 'Jonathan ',
      apellido: 'Valiente',
      email: 'jonathanv@gmail.com',
      fechaIngreso: '23/04/2010'
    },
    {
      nombre: 'David ',
      apellido: 'Valiente',
      email: 'bdvalientes@gmail.com',
      fechaIngreso: '23/04/2010'
    }
  ];


  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  // metodos
  // this.navigationExtras - pasar la informacion a travez de la ruta
  onGoToEdit(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['edit'], this.navigationExtras);
  }

  onGoToSee(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['detailsu'], this.navigationExtras);
  }

  onGoToDelete(item: any): void {
    // alert('Deleted');
    alert('Usuario Eliminado de la lista');

  }

}
