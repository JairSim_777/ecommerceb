import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsuRoutingModule } from './detailsu-routing.module';
import { DetailsuComponent } from './detailsu.component';


@NgModule({
  declarations: [
    DetailsuComponent
  ],
  imports: [
    CommonModule,
    DetailsuRoutingModule
  ]
})
export class DetailsuModule { }
