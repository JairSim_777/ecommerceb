import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Dtuser } from 'src/app/shared/components/models/dtuser.interface';

@Component({
  selector: 'app-detailsu',
  templateUrl: './detailsu.component.html',
  styleUrls: ['./detailsu.component.scss']
})
export class DetailsuComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  // tipo any por que no se ha creado la interfaz
  // dtuser: any = null;

  // interfaz
  dtuser: Dtuser = null;

  constructor(private router: Router) {
    const navigation = this.router.getCurrentNavigation();
    // permite pintar los datos en el formulario
    this.dtuser = navigation?.extras?.state?.value;
  }

  // Si se recarga la pagina nuevamente estando en detailsu
  ngOnInit(): void {
    if(typeof this.dtuser === 'undefined'){
      // Dirigase a List
      this.router.navigate(['List']);
    }
  }

  onGoToEdit(): void {
    this.navigationExtras.state.value = this.dtuser;
    // se dirige al componente edit boton de la lista usuarios)
    this.router.navigate(['edit'], this.navigationExtras);
  }

  onDelete(): void {
    // alert('Deleted');
    alert('Usuario Eliminado detalles');
  }

  onGoBackToList(): void{
    // regresar a la ruta List
    this.router.navigate(['List']);
  }

}
