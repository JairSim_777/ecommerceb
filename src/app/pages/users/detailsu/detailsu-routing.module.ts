import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsuComponent } from './detailsu.component';

const routes: Routes = [{ path: '', component: DetailsuComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsuRoutingModule { }
