import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'Home',
    loadChildren: () =>import('./pages/home/home.module').then(m => m.HomeModule)
  },
  {
    // Redirect by default to Home
    path: '',
    redirectTo: 'Home',
    pathMatch: 'full'
  },

  { path: 'List', loadChildren: () => import('./pages/users/list/list.module').then(m => m.ListModule) },
  { path: 'edit', loadChildren: () => import('./pages/users/edit/edit.module').then(m => m.EditModule) },
  { path: 'registeru', loadChildren: () => import('./pages/users/registeru/registeru.module').then(m => m.RegisteruModule) },
  { path: 'detailsu', loadChildren: () => import('./pages/users/detailsu/detailsu.module').then(m => m.DetailsuModule) },
  { path: 'newu', loadChildren: () => import('./pages/users/newu/newu.module').then(m => m.NewuModule) },
  { path: 'login', loadChildren: () => import('./pages/auth/login/login.module').then(m => m.LoginModule) },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
